# Video conversion script
This .sh script recursively converts all videos in a folder to .mp4 (h264 codec) videos and copys the metadata.
This script is written for macOS. It is rather simple and has a lot of room for improvement, still, I hope that this script might help others to get started quickly.

## Why?
I wanted to import and archive my old video files with different codec and formats to the mac photos library. Therefore, the metadata of the files should be preserved and the files should be converted into a modern,macOS-readable format.


## Getting started
This is a .sh script that uses ffmpeg and exiftool, so you might want to install the tools first f.e. by using brew on macOS:
```
brew install ffmpeg
```
```
brew install exiftool
```

## Usage
You should first test if your video files and the conversion match. You probably want to check the codec, and format as well as the metadata and change the script accordingly.

### Video 
Check the input codec and format by: 
```
ffmpeg -i inputfile.avi
```
### Metadata check:
Check, which creation date is correct, by using:
```
exiftool -s -time:all inputfile.avi
```
The script can be used like this:
```
bash ./movie-convert.sh /foldertovideos/
```