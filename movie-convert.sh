#!/bin/bash

RDIR=$1

# fill with more extensions if needed, stick to the naming-convention
TYPES="(AVI|mov|avi|mp4|VOB)"

# convert the file using ffmpeg and change metadata using exiftool
function convert(){
    FILE=$1
    FILENEW=${FILE}_converted.mp4

    echo $FILE

    # change the params depending on input videos
    # high qualitiy: -b:v 4000k
    ffmpeg -i "${FILE}" -pix_fmt yuv420p -b:v 2000k -c:v libx264 -map_metadata 0 -movflags use_metadata_tags "${FILENEW}" -hide_banner -loglevel error;
    STATUS1=$?
    # change the params depending on the input metadata...
    exiftool -TagsFromFile "${FILE}" "-all:all>all:all" "${FILENEW}";
    STATUS2=$?
    exiftool -tagsFromFile "${FILE}" -FileModifyDate "${FILENEW}";
    STATUS3=$?

    # here, you can define if the original file should be removed, if conversion is successful
    # use at own risk
    if [ $STATUS1 -eq 0 ] && [ $STATUS2 -eq 0 ] && [ $STATUS3 -eq 0 ]
    then
        echo "No Error found, delete old file.."
        #rm "${FILE}"
    else
        echo "Error found, leaving old file"
    fi
}
export -f convert

# find all video files in folder...
vids= find -E "$RDIR" -regex ".*\.${TYPES}" -exec bash -c 'convert "$1"' _ {} \; 
